<?xml version="1.0"  encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:usage="http://www.energyinputlabeling.org/EILUsageXML.2009.1"
		xmlns:files="http://ggg.ctrl-alt-del.si/xmlns/filelist"
                version="1.0">

<xsl:output     method="xml" 
                indent="yes"/>

<xsl:template match="input">
<totals for="{name}" date="{date}">
 <all>
  <xsl:for-each select="item">
   <xsl:variable name="resource" select="resource"/>
   <xsl:variable name="counthours" select="count * hours"/>

   <xsl:for-each select="document('dynamic/itemlist.xml')/usage:EILUsage/usage:item">
    <xsl:if test="usage:name != preceding-sibling::usage:item[1]/usage:name">
     <xsl:if test="usage:name = $resource">
      <result resource="{$resource}"><xsl:value-of select="number(usage:hour/usage:MJ * $counthours)"/></result>
     </xsl:if>
    </xsl:if>
   </xsl:for-each>

  </xsl:for-each>
 </all>

 <human>
  <xsl:for-each select="item">
   <xsl:variable name="resource" select="resource"/>
   <xsl:variable name="counthours" select="count * hours"/>

   <xsl:for-each select="document('dynamic/itemlist.xml')/usage:EILUsage/usage:item">
    <xsl:if test="usage:name != preceding-sibling::usage:item[1]/usage:name">
     <xsl:if test="(usage:name = $resource) and (usage:type/@human = '1')">
      <result resource="{$resource}"><xsl:value-of select="number(usage:hour/usage:MJ * $counthours)"/></result>
     </xsl:if>
    </xsl:if>
   </xsl:for-each>

  </xsl:for-each>
 </human>

 <electric>
  <xsl:for-each select="item">
   <xsl:variable name="resource" select="resource"/>
   <xsl:variable name="counthours" select="count * hours"/>

   <xsl:for-each select="document('dynamic/itemlist.xml')/usage:EILUsage/usage:item">
    <xsl:if test="usage:name != preceding-sibling::usage:item[1]/usage:name">
     <xsl:if test="(usage:name = $resource) and (usage:type/@electric = '1')">
      <result resource="{$resource}"><xsl:value-of select="number(usage:hour/usage:MJ * $counthours)"/></result>
     </xsl:if>
    </xsl:if>
   </xsl:for-each>

  </xsl:for-each>
 </electric>
</totals>
</xsl:template>
</xsl:stylesheet>
