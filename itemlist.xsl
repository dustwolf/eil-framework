<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.


      This file generates a list of all item names in the databases.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:files="http://ggg.ctrl-alt-del.si/xmlns/filelist"
                xmlns:e="http://www.energyinputlabeling.org/EILUsageXML.2009.1"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                indent="no" />

<xsl:template match="files:files">
<e:EILUsage>

 <xsl:for-each select="document(files:file)/e:EILUsage/e:item">
  <xsl:sort select="e:name" />
  <e:item>
   <e:name><xsl:value-of select="e:name" /></e:name>
   <xsl:if test="e:type/@electric"><e:type electric="1"/></xsl:if>
   <xsl:if test="e:type/@human"><e:type human="1"/></xsl:if>
   <e:W><xsl:value-of select="e:W" /></e:W>
   <e:hour>
    <e:J><xsl:value-of select="e:hour/e:J" /></e:J>
    <e:MJ><xsl:value-of select="e:hour/e:MJ" /></e:MJ>
   </e:hour>
  </e:item>
 </xsl:for-each>

</e:EILUsage>
</xsl:template>

</xsl:stylesheet>
