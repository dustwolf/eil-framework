<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.


      This file generates the XForms code from the previous database.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xf="http://www.w3.org/2002/xforms"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:e="http://www.w3.org/2001/xml-events"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                omit-xml-declaration="yes"
                indent="yes" />

<xsl:template match="databaseEditor">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
  <meta content="text/html; charset=UTF-8"
        http-equiv="content-type" />
  <title>Energy Input Label generator - Usage database editor</title>
  <xf:model>
   
   <!-- Form data (filled in when form displayed) -->
   <xf:instance>
    <xsl:copy>
     <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
   </xf:instance>

   <!-- Where to post to -->
   <xf:submission id="form" method="xml-urlencoded-post" replace="all" action="{postTo}" />

   <!-- Data type defenitions -->
   <xf:bind nodeset="item/name" type="xs:string" />
   <xf:bind nodeset="item/type" type="xs:string" />
   <xf:bind nodeset="item/W" type="xs:float"/>

  </xf:model>
</head>
<body>

 <xf:repeat nodeset="item" id="items">

  <xf:input ref="name">
   <xf:label>Resource: </xf:label>
  </xf:input>,
 
  <xf:select1 ref="type">
   <xf:label> Type: </xf:label>
   <xf:item>
    <xf:label>Electric</xf:label>
    <xf:value>electric</xf:value>
   </xf:item>
   <xf:item>
    <xf:label>Human</xf:label>
    <xf:value>human</xf:value>
   </xf:item>
  </xf:select1>,

  <xf:input ref="W">
   <xf:label>Nominal power input (W): </xf:label>
  </xf:input>,

  <xf:trigger>
   <xf:label>Delete item</xf:label>
   <xf:delete nodeset="." at="1" e:event="DOMActivate"
              if="count(//item) > 1" />
  </xf:trigger>
  
 </xf:repeat>
 <br/>
 <xf:trigger>
  <xf:label>New item</xf:label>
  <xf:insert nodeset="item" at="last()"
             position="after" e:event="DOMActivate" />
 </xf:trigger>

 <xf:submit submission="form">
  <xf:label>Save</xf:label>
 </xf:submit>

 <br/><br/><br/>
 <a href="../form.php">Go back to the form</a>.

</body>
</html>
</xsl:template>

<!-- this makes sure the input data remains intact -->
<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
