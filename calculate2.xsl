<?xml version="1.0"  encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.energyinputlabeling.org/EILXML.2009.1"
                version="1.0">

<xsl:output     method="xml" 
                indent="yes"/>

<xsl:template match="totals">
<EIL for="{@for}" date="{@date}">

 <xsl:variable name="total" select="sum(all/result)" />
 <xsl:variable name="human" select="sum(human/result)"/>
 <xsl:variable name="electric" select="sum(electric/result)"/>

 <MJ>
  <electric><xsl:value-of select="$electric"/></electric>
  <human><xsl:value-of select="$human"/></human>
  <total><xsl:value-of select="$total"/></total>
 </MJ>
 <percent>
  <electric><xsl:value-of select="round(($electric * 100) div $total)"/></electric>
  <human><xsl:value-of select="round(($human * 100) div $total)"/></human>
 </percent>

</EIL>
</xsl:template>
</xsl:stylesheet>
