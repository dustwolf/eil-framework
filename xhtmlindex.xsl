<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.


      This is simply copied from:
      http://info.ctrl-alt-del.si/xhtmlindex.xsl

      It is used to display file lists in the XML namespace:
      http://ggg.ctrl-alt-del.si/xmlns/filelist

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:files="http://ggg.ctrl-alt-del.si/xmlns/filelist"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8" />

<xsl:template match="files:files">
<html xmlns="http://www.w3.org/1999/xhtml" lang="sl">
 <head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title></title>
 </head>
 <body>
  <h1>Index of local XML data sources</h1>
  <hr style="width: 100%; height: 2px;" /><br />
  <ul>
   <xsl:for-each select="files:file">
    <xsl:sort select="@name" />
    <li><a href="{.}"><xsl:value-of select="@name" /></a></li>
   </xsl:for-each>
  </ul><br />
  <hr style="width: 100%; height: 2px;" />
  <span style="font-style: italic;">Powered by XSLT</span><br />
 </body>
</html>
</xsl:template>

</xsl:stylesheet>
