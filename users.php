<?php header("Content-type: application/xml"); ?>
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet href="xsltforms/xsltforms.xsl" type="text/xsl"?>
<?php
/*

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.
*/

 //Saves feedback if any
 $xml = file_get_contents("php://input");
 if(substr($xml,0,9) == "postdata=") {
  $xml = urldecode(substr($xml,strpos($xml,"=")+1));
  $fn = fopen("dynamic/passwd.xml", "w");
  fwrite($fn,$xml);
  fclose($fn);
 }

 //Process input here

 //Generates XForms markup from current input
 passthru("xsltproc form.xsl dynamic/passwd.xml");
?>

