<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.


      This file generates the XForms code from the previous input.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:files="http://ggg.ctrl-alt-del.si/xmlns/filelist"
		xmlns:eil="http://www.energyinputlabeling.org/EILUsageXML.2009.1"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                omit-xml-declaration="yes"
                indent="yes" />

<xsl:template match="input">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
 <title>Energy Input Label generator</title>
</head>
<body>
<h1>Energy Input Label bill</h1>
<table style="text-align: left; width: 350px;" border="0" cellpadding="2" cellspacing="2">
 <tbody>
  <tr>
   <td><b>Resource</b></td>
   <td><b>Count</b></td>
   <td><b>Duration (h)</b></td>
  </tr>
  <xsl:for-each select="item">
   <tr>
    <td><xsl:value-of select="resource" /></td>
    <td><xsl:value-of select="count" /></td>
    <td><xsl:value-of select="hours" /></td>
   </tr>
  </xsl:for-each>
 </tbody>
</table>
<br />
<img src="http://pritisni.ctrl-alt-del.si/NET/eil-framework/labelpng.php" alt="Energy Input Label"/>
</body>
</html>

</xsl:template>

</xsl:stylesheet>
