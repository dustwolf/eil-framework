<?php 
/*

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.

      
      This file is the database editor (GUI)

      Since some data is hard to pass, generate XML containing the filename
      and some easily parsable indication on where to post feedback
      ("userdefined") and use this XML with a converter that changes the 
      database and the post destination into something more usable with an 
      XForms converter.

      When recieving content, put it trough a converter to generate an
      actual database again, then overwrite.
*/

//http://www.example.com/database.php/userdefined or http://www.example.com/database.php/companywide
$src=substr(escapeshellcmd(rawurldecode($_SERVER["PATH_INFO"])),1);
$src=str_replace(".","",$src); $src=str_replace("/","",$src);

if( strpos($src,"userdefined")===False and strpos($src,"companywide")===False ) {
 header("Location: database.php/userdefined");
 die();
}
header("Content-type: application/xml"); ?>
<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet href="../xsltforms/xsltforms.xsl" type="text/xsl"?>
<?php
 //Saves feedback if any
 $xml = file_get_contents("php://input");
 if(substr($xml,0,9) == "postdata=") {
  $xml = urldecode(substr($xml,strpos($xml,"=")+1));
  exec("echo ".escapeshellarg($xml)."| xsltproc databasesave.xsl - > dynamic/".$src.".xml");
 }

 //Generates XForms markup from current input
 $commandxml = '<?xml version="1.0" encoding="UTF-8"?>'."\n".'<files xmlns="http://ggg.ctrl-alt-del.si/xmlns/filelist"><file name="'.$src.'">dynamic/'.$src.'.xml</file></files>';
 exec("echo ".escapeshellarg($commandxml)." | xsltproc databaseedit.xsl - > dynamic/databaseedit.xml"); //Generates editable content
 passthru("xsltproc database.xsl dynamic/databaseedit.xml");
?>
