<?php
/*

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.
*/

 header("Content-type: image/svg+xml");
 header("Cache-Control: no-cache, must-revalidate"); //Dynamic so don't cache
 passthru("xsltproc calculate1.xsl dynamic/input.xml | xsltproc calculate2.xsl - | xsltproc label.xsl -");
?>
