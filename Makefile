all:
	# Use make install to install xsltforms
	# Use make clean to remove xsltforms

install:
	make clean
	wget http://downloads.sourceforge.net/project/xsltforms/xsltforms/Beta%202/xsltforms-beta2.zip
	unzip xsltforms-beta2.zip -d xsltforms
	-rm xsltforms-beta2.zip
	-rm xsltforms/*
	mv xsltforms/xsltforms/* xsltforms/
	rm -r xsltforms/xsltforms
	cp messages.properties xsltforms/messages.properties

clean:
	-rm xsltforms-beta2.zip
	-rm -r xsltforms
