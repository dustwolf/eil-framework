<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.


      This file takes a file list (of 1 file) and generates 
      database editor markup.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:files="http://ggg.ctrl-alt-del.si/xmlns/filelist"
                xmlns:eil="http://www.energyinputlabeling.org/EILUsageXML.2009.1"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                indent="yes" />

<xsl:template match="files:files">
<databaseEditor>
 <postTo><xsl:value-of select="files:file/@name" /></postTo> <!-- filename -->
 <xsl:for-each select="document(files:file)/eil:EILUsage/eil:item">
  <item>
   <name><xsl:value-of select="eil:name" /></name>
   <type><xsl:if test="eil:type/@electric">electric</xsl:if><xsl:if test="eil:type/@human">human</xsl:if></type> <!-- This should make for some funny results when both are true -->
   <W><xsl:value-of select="eil:W" /></W>
  </item>
 </xsl:for-each>

</databaseEditor>
</xsl:template>

</xsl:stylesheet>
