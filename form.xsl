<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.


      This file generates the XForms code from the previous input.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xf="http://www.w3.org/2002/xforms"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
		xmlns:e="http://www.w3.org/2001/xml-events"
		xmlns:files="http://ggg.ctrl-alt-del.si/xmlns/filelist"
		xmlns:eil="http://www.energyinputlabeling.org/EILUsageXML.2009.1"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                omit-xml-declaration="yes"
                indent="yes" />

<xsl:template match="input">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
  <meta content="text/html; charset=UTF-8"
        http-equiv="content-type" />
  <title>Energy Input Label generator - User data input</title>
  <link href="form.css" rel="stylesheet" type="text/css" />

  <xf:model>

   <!-- Form data (filled in when form displayed) -->
   <xf:instance>
    <xsl:copy>
     <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
   </xf:instance>

   <!-- Where to post to -->
   <xf:submission id="form" method="xml-urlencoded-post" replace="all" action="form.php" />

   <!-- Data type defenitions -->
   <xf:bind nodeset="name" type="xs:string" />
   <xf:bind nodeset="date" type="xs:date" />
   <xf:bind nodeset="item/count" type="xs:decimal" />
   <xf:bind nodeset="item/hours" type="xs:decimal" />
   <xf:bind nodeset="item/resource" type="xs:string" />

  </xf:model>
</head>
<body>

 <div id="menu">
  <ul>
   <!-- Provide download links for the intermediate stuff -->
   <li><h2>XML data download links</h2></li>
   <ul class="XMLdata">
    <li><a href="dynamic/input.xml">Form data</a></li>
    <li><a href="databases.xml">usage databases</a></li>
    <li><a href="EILMarkup.php">Energy Input Label markup</a></li>
   </ul>
  
   <!-- Provide links for editing the databases -->
   <li><h2>Edit resource databases</h2></li>
   <ul class="editDatabases">
    <li><a href="database.php/userdefined">User-defined resource database</a></li>
    <li><a href="database.php/companywide">Company-wide resource database</a></li>
   </ul>
  
   <!-- MISC -->
   <li><h2>See also</h2></li>
   <ul class="otherLinks">
    <li><a href="billformat.php" target="_blank">Bill format</a></li>
    <li><a href="label.php" target="_blank">Label in SVG format</a></li>
   </ul>
  </ul>
 </div>

 <!-- Form appearance -->
 
 <xf:input ref="name">
  <xf:label>Process name: </xf:label>
 </xf:input>&#160; 
 <xf:input ref="date">
  <xf:label>Date: </xf:label>
 </xf:input><br/><br/>

 <xf:repeat nodeset="item" id="items">

  <xf:select1 ref="resource">
   <xf:label>Resource: </xf:label>
   <xsl:for-each select="document('dynamic/itemlist.xml')/eil:EILUsage/eil:item">
    <xsl:if test="eil:name != preceding-sibling::eil:item[1]/eil:name">
     <xf:item>
      <xf:label><xsl:value-of select="eil:name" /> (<xsl:value-of select="eil:W"/>W)</xf:label>
      <xf:value><xsl:value-of select="eil:name" /></xf:value>
     </xf:item>
    </xsl:if>
   </xsl:for-each>

  </xf:select1>&#160; 

  <xf:input ref="count">
   <xf:label>Number used: </xf:label>
  </xf:input>&#160; 
  <xf:input ref="hours">
   <xf:label>Hours spent: </xf:label>
  </xf:input>&#160; 

  <xf:trigger>
   <xf:label>Delete item</xf:label>
   <xf:delete nodeset="." at="1" e:event="DOMActivate"
              if="count(//item) > 1" />
  </xf:trigger>
  <br/>
  
 </xf:repeat>
 <br/>

 <xf:trigger>
  <xf:label>New item</xf:label>
  <xf:insert nodeset="item" at="last()"
             position="after" e:event="DOMActivate" />
 </xf:trigger>

 <xf:submit submission="form">
  <xf:label>Save</xf:label>
 </xf:submit>

<!-- Also embed label -->
<br/><br/>
<img src="http://pritisni.ctrl-alt-del.si/NET/eil-framework/labelpng.php" alt="Energy Input Label"/>
</body>
</html>

</xsl:template>

<!-- this makes sure the input data remains intact -->
<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
