#
#     Copyright 2009 Jure Sah
#
#     This file is part of EIL framework.
#
#     EIL framework is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     Please refer to the README file for additional information.
#
xsltproc calculate1.xsl inputSample.xml | xsltproc calculate2.xsl - | xsltproc labelHumanElectric.xsl - | xsltproc label.xsl - | rsvg-convert > EILSample2.png
