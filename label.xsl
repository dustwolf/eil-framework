<?xml version="1.0"  encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xlink="http://www.w3.org/1999/xlink"
		xmlns:eil="http://www.energyinputlabeling.org/EILXML.2009.1"
                version="1.0">

<xsl:output     method="xml"
		encoding="UTF-8" 
                doctype-system="http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd" 
                doctype-public="-//W3C//DTD SVG 1.0//EN"
                indent="yes"/>

<xsl:template match="eil:EIL" xmlns="http://www.w3.org/2000/svg">
<xsl:variable name="width">590</xsl:variable>
<xsl:variable name="lines">6</xsl:variable>
<xsl:variable name="fSize">28</xsl:variable>
<xsl:variable name="height" select="$lines * $fSize + 20" />

 <svg width="{$width+20}" height="{($lines)*$fSize+35}">

  <!-- Label frame -->
  <rect x="10" y="10" width="{$width}" height="{($lines)*$fSize+20}" 
        style="fill:white;stroke-width:1;stroke:rgb(0,0,0);stroke-dasharray: 9, 5;"/>

  <!-- Circles in the background -->
  <xsl:variable name="sizefactor" select="0.5" />

  <xsl:choose>
   <xsl:when test="eil:MJ/eil:electric &lt; 4">
    <circle cx="{($width div 2)+5-($width div 4)}" cy="{($height div 2)*0.7}" r="{$sizefactor*eil:percent/eil:electric}" stroke="opacity:0" stroke-width="0" fill="rgb(64,192,64)" />
   </xsl:when>
   <xsl:when test="(eil:MJ/eil:electric &gt;= 4) and (eil:MJ/eil:electric &lt; 68)">
    <xsl:variable name="yellowness" select="round((eil:MJ/eil:electric - 4) * 2)"/>
    <circle cx="{($width div 2)+5-($width div 4)}" cy="{($height div 2)*0.7}" r="{$sizefactor*eil:percent/eil:electric}" stroke="opacity:0" stroke-width="0" fill="rgb({64 + $yellowness},192,64)" />
   </xsl:when>
   <xsl:when test="(eil:MJ/eil:electric &gt;= 68) and (eil:MJ/eil:electric &lt; 132)">
    <xsl:variable name="redness" select="round((eil:MJ/eil:electric - 68) * 2)"/>
    <circle cx="{($width div 2)+5-($width div 4)}" cy="{($height div 2)*0.7}" r="{$sizefactor*eil:percent/eil:electric}" stroke="opacity:0" stroke-width="0" fill="rgb(192,{192 - $redness},64)" />
   </xsl:when>
   <xsl:otherwise>
    <circle cx="{($width div 2)+5-($width div 4)}" cy="{($height div 2)*0.7}" r="{$sizefactor*eil:percent/eil:electric}" stroke="opacity:0" stroke-width="0" fill="rgb(192,64,64)" />
   </xsl:otherwise>
  </xsl:choose>

  <xsl:choose>
   <xsl:when test="eil:MJ/eil:human &lt; 1">
    <circle cx="{($width div 2)+5+($width div 5)}" cy="{($height div 2)*1.3}" r="{$sizefactor*eil:percent/eil:human}" stroke="opacity:0" stroke-width="0" fill="rgb(64,192,64)" />
   </xsl:when>
   <xsl:when test="(eil:MJ/eil:human &gt;= 1) and (eil:MJ/eil:human &lt; 33)">
    <xsl:variable name="yellowness" select="round((eil:MJ/eil:human - 4) * 4)"/>
    <circle cx="{($width div 2)+5+($width div 5)}" cy="{($height div 2)*1.3}" r="{$sizefactor*eil:percent/eil:human}" stroke="opacity:0" stroke-width="0" fill="rgb({64 + $yellowness},192,64)" />
   </xsl:when>
   <xsl:when test="(eil:MJ/eil:human &gt;= 33) and (eil:MJ/eil:human &lt; 65)">
    <xsl:variable name="redness" select="round((eil:MJ/eil:human - 33) * 4)"/>
    <circle cx="{($width div 2)+5+($width div 5)}" cy="{($height div 2)*1.3}" r="{$sizefactor*eil:percent/eil:human}" stroke="opacity:0" stroke-width="0" fill="rgb(192,{192 - $redness},64)" />
   </xsl:when>
   <xsl:otherwise>
    <circle cx="{($width div 2)+5+($width div 5)}" cy="{($height div 2)*1.3}" r="{$sizefactor*eil:percent/eil:human}" stroke="opacity:0" stroke-width="0" fill="rgb(192,64,64)" />
   </xsl:otherwise>
  </xsl:choose>

  <!-- QR code -->
  <image xlink:href="qrcode.php?q=http://pritisni.ctrl-alt-del.si/NET/eil-framework/EILMarkup.php"
         x="{$width - 82}" y="15"
         width="82" height="82" /> 
  <image xlink:href="qrcode.php?q=http://pritisni.ctrl-alt-del.si/NET/eil-framework/dynamic/input.xml"
         x="{$width - 82*2}" y="15"
         width="82" height="82" />
 
  <!-- Text of label -->
  <xsl:variable name="fStyle">font-size:<xsl:value-of select="$fSize" />px;font-family:FreeMono;line-height:125%</xsl:variable>

  <text text-anchor="start" x="20" y="{$fSize+15}" style="{$fStyle}"><xsl:value-of select="/eil:EIL/@for"/></text>
  <text text-anchor="start" x="20" y="{$fSize*2+15}" style="{$fStyle}"><xsl:value-of select="/eil:EIL/@date"/></text>

  <text text-anchor="start" x="20" y="{$fSize*4+15}" style="{$fStyle}">Energy Input:</text>
  <text text-anchor="end" x="{$width*0.8}" y="{$fSize*4+15}" style="{$fStyle}">
   <xsl:value-of select="format-number(eil:MJ/eil:total, '0.000')"/> MJ</text>
  <text text-anchor="end" x="{$width}" y="{$fSize*4+15}" style="{$fStyle}">(100%)</text>

  <text text-anchor="start" x="20" y="{$fSize*5+15}" style="{$fStyle}">Electric power</text>
  <text text-anchor="end" x="{$width*0.8}" y="{$fSize*5+15}" style="{$fStyle}">
   <xsl:value-of select="format-number(eil:MJ/eil:electric, '0.000')"/> MJ</text>
  <text text-anchor="end" x="{$width}" y="{$fSize*5+15}" style="{$fStyle}">(<xsl:value-of select="round(eil:percent/eil:electric)"/>%)</text>

  <text text-anchor="start" x="20" y="{$fSize*6+15}" style="{$fStyle}">Human power</text>
  <text text-anchor="end" x="{$width*0.8}" y="{$fSize*6+15}" style="{$fStyle}">
   <xsl:value-of select="format-number(eil:MJ/eil:human, '0.000')"/> MJ</text>
  <text text-anchor="end" x="{$width}" y="{$fSize*6+15}" style="{$fStyle}">(<xsl:value-of select="round(eil:percent/eil:human)"/>%)</text>

 </svg> 
 </xsl:template>
</xsl:stylesheet>
