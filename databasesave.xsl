<?xml version="1.0" encoding="UTF-8"?>
<!-- 

      Copyright 2009 Jure Sah

      This file is part of EIL framework.

      EIL framework is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      Please refer to the README file for additional information.


      This file takes a file list (of 1 file) and generates 
      database editor markup.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:e="http://www.energyinputlabeling.org/EILUsageXML.2009.1"
                version="1.0">

<xsl:output     method="xml"
                encoding="UTF-8"
                indent="yes" />

<xsl:template match="databaseEditor">
<e:EILUsage>
 <xsl:for-each select="item">
  <e:item>
   <e:name><xsl:value-of select="name" /></e:name>
   <xsl:if test="type='electric'"><e:type electric="1" /></xsl:if>
   <xsl:if test="type='human'"><e:type human="1" /></xsl:if>
   <e:W><xsl:value-of select="W" /></e:W>
   <e:hour>
    <e:J><xsl:value-of select="W * 3600" /></e:J>
    <e:MJ><xsl:value-of select="W * 0.0036" /></e:MJ>
   </e:hour>
  </e:item>
 </xsl:for-each>
</e:EILUsage>
</xsl:template>

</xsl:stylesheet>
